import { Typography } from "@mui/material";
import * as React from "react";

export interface IHomeProps {}

export default function Home(props: IHomeProps) {
  let localDB: any = localStorage.getItem("user_info");
  localDB = JSON.parse(localDB);
  return (
    <div className="text-center">
      <Typography className="pt-10" variant="h4" gutterBottom component="div">
        Hey {localDB.firstName + localDB.lastName}!
      </Typography>
      <Typography className="text-lg" variant="subtitle1" component="div">
        Welcome to the project page
      </Typography>
    </div>
  );
}
