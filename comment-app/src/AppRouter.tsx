import * as React from "react";
import { Routes, Route } from "react-router-dom";
import Auth from "./authentication/Auth";
import Home from "./homeModule/Home";

export const AppRouter = () => (
  <Routes>
    <Route path="/login" element={<Auth />} />
    <Route path="/home" element={<Home />} />
  </Routes>
);
