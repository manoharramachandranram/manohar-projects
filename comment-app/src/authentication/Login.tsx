import * as React from "react";
import { Avatar, Button, Grid, TextField, Typography } from "@mui/material";
import { Box } from "@mui/system";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import { useNavigate } from "react-router";

export interface ILoginProps {
  handleSignup(
    arg0: boolean
  ): React.MouseEventHandler<HTMLButtonElement> | undefined;
}

export default function Login(props: ILoginProps) {
  const navigate = useNavigate();
  const [loginData, setLoginData] = React.useState({
    email: "",
    password: "",
  });

  const [errorMsg, setErrorMsg] = React.useState("");

  const handleChange = (evt: any) => {
    setLoginData({ ...loginData, [evt.target.name]: evt.target.value });
  };

  const handleLogin = (): void => {
    console.log("some thing");
    let localDB: any = localStorage.getItem("user_info");
    localDB = JSON.parse(localDB);
    console.log(localDB, "localDB");
    if (loginData.email !== localDB.email) {
      setErrorMsg("Email is incorrect");
    } else if (loginData.password !== localDB.password) {
      setErrorMsg("Password is incorrect");
    } else {
      setErrorMsg("");
      navigate("/home");
    }
  };

  return (
    <Box
      sx={{
        my: 8,
        mx: 4,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component="h1" variant="h5">
        Sign in
      </Typography>
      <Box component="form" noValidate sx={{ mt: 1 }}>
        <TextField
          margin="normal"
          required
          fullWidth
          id="email"
          label="Email Address"
          name="email"
          autoComplete="email"
          autoFocus
          onChange={handleChange}
        />
        <TextField
          margin="normal"
          required
          fullWidth
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="current-password"
          onChange={handleChange}
        />

        <Button
          fullWidth
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
          onClick={handleLogin}
        >
          Sign In
        </Button>
        {errorMsg !== "" && (
          <Typography
            variant="subtitle1"
            gutterBottom
            component="div"
            className="w-full text-red-400 text-center"
          >
            {errorMsg}
          </Typography>
        )}
        <Grid container>
          <Grid item xs></Grid>
          <Grid item>
            <Button
              variant="text"
              size="small"
              onClick={props.handleSignup(false)}
            >
              Don't have an account? Sign Up
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}
