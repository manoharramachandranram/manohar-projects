import * as React from "react";
import { ThemeProvider } from "@emotion/react";
import { createTheme, Grid, Paper, Typography } from "@mui/material";
import Login from "./Login";
import CopyRight from "./CopyRight";
import Register from "./Register";

const theme = createTheme();

export interface IAuthProps {}

export default function Auth(props: IAuthProps) {
  const [isSignin, setIsSignIn] = React.useState(true);

  const handleChange = (isLogin: boolean) => (): void => {
    if (isLogin) {
      setIsSignIn(true);
    } else {
      setIsSignIn(false);
    }
  };
  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage: "url(https://wallpapercave.com/wp/wp4974476.jpg)",
            backgroundRepeat: "no-repeat",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
          className="flex w-full h-full items-center justify-center "
        >
          <div>
            <Typography
              variant="h2"
              gutterBottom
              component="div"
              className="w-full text-white"
            >
              Welcome to My Projects!
            </Typography>
            <Typography
              variant="subtitle1"
              gutterBottom
              component="div"
              className="w-full text-white"
            >
              Hey! Here You will find out the more projects related to the Front
              End... (React type script)
            </Typography>
          </div>
        </Grid>
        <Grid
          item
          xs={12}
          sm={8}
          md={5}
          component={Paper}
          elevation={6}
          square
          className="relative"
        >
          {isSignin ? (
            <Login handleSignup={handleChange} />
          ) : (
            <Register handleLogin={handleChange} />
          )}
          <div className="absolute bottom-5  flex justify-center w-full">
            <CopyRight />
          </div>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
