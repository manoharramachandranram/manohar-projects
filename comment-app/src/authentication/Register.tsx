import * as React from "react";
import { Avatar, Button, Grid, TextField, Typography } from "@mui/material";
import { Box } from "@mui/system";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import { useNavigate } from "react-router-dom";
import { json } from "stream/consumers";

export interface IRegisterProps {
  handleLogin(
    arg0: boolean
  ): React.MouseEventHandler<HTMLButtonElement> | undefined;
}

export default function Register(props: IRegisterProps) {
  const navigate = useNavigate();
  const [registerData, setRegisterData] = React.useState({
    email: "",
    password: "",
    firstName: "",
    lastName: "",
  });

  const handleChange = (evt: any) => {
    console.log(evt, "evt");
    const value: any = evt.target.value;
    setRegisterData({ ...registerData, [evt.target.name]: value });
  };

  const submitSignupForm = () => {
    localStorage.setItem("user_info", JSON.stringify(registerData));

    navigate("/home");
  };

  return (
    <Box
      sx={{
        my: 8,
        mx: 4,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component="h1" variant="h5">
        Sign Up
      </Typography>
      <Box component="form" noValidate sx={{ mt: 1 }}>
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="firstName"
              label="First Name"
              name="firstName"
              autoComplete="name"
              autoFocus
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="lastName"
              label="Last Name"
              name="lastName"
              autoComplete="name"
              autoFocus
              onChange={handleChange}
            />
          </Grid>
        </Grid>
        <TextField
          margin="normal"
          required
          fullWidth
          id="email"
          label="Email Address"
          name="email"
          autoComplete="email"
          autoFocus
          onChange={handleChange}
        />
        <TextField
          margin="normal"
          required
          fullWidth
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="current-password"
          onChange={handleChange}
        />

        <Button
          fullWidth
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
          onClick={submitSignupForm}
        >
          Sign Up
        </Button>

        <Grid container>
          <Grid item xs></Grid>
          <Grid item>
            <Button
              variant="text"
              size="small"
              onClick={props.handleLogin(true)}
            >
              Have an account? Sign in
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}
