import { Typography } from "@mui/material";
import * as React from "react";

export interface ICopyRightProps {}

export default function CopyRight(props: ICopyRightProps) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      Manohar Ramachandran {new Date().getFullYear()}
    </Typography>
  );
}
